package model;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by controlberkani on 22/12/2015.
 */
public class GliImpltest {
    static BoardImpl board ;
    static BoardImpl secondeBoard;

    @Before
    public void New(){
        board =  new BoardImpl(3);
    }
    // test if we can add random Tile
    @Test
    public void addRandom(){
        // assert statements
        int Old,New;
        Old=numbreTile(board.getBoard());
        board.packIntoDirection(Board.Direction.RIGHT);
        board.commit();
        New = numbreTile(board.getBoard());
        assertTrue(Old <= New);
        //assertNotNull(board);

    }
    // Test if board pack into right
    @Test
    public void packIntoDirection(){
        board.loadBoard(new int[][]{{1, 0, 0}, {0, 0, 0}, {0, 0, 0}});
        assertNotNull(board.getTile(1, 1));
        board.packIntoDirection(Board.Direction.RIGHT);
        board.commit();
        assertNotNull(board.getTile(1,3));
    }
    // Test if the Tile in (1,3) is moved to (1,1)
    @Test
    public void packIntoDirection2(){
        board.loadBoard(new int[][]{{0, 0, 1}, {0, 0, 0}, {0, 0, 0}});
        assertNotNull(board.getTile(1,3));
        board.packIntoDirection(Board.Direction.LEFT);
        board.commit();
        assertNotNull(board.getTile(1,1));
    }
    /*
     * Check if a basic merge works,
     *Test if that Tiles in (1,2) and (1,3) get merged into (1,1)
      */
    @Test
    public void packIntoDirection3(){
        board.loadBoard(new int[][]{{0, 1, 1}, {0, 0, 0}, {0, 0, 0}});
        assertNotNull(board.getTile(1,3));
        assertNotNull(board.getTile(1,2));
        board.packIntoDirection(Board.Direction.LEFT);
        board.commit();
        assertNotNull(board.getTile(1,1));
        assertEquals(2,board.getTile(1,1).getRank());
    }
    /*
         * Check if a complicated merge works,
         *Test if that Tiles in (1,2) and (1,3) get merged into (1,2)
          */
    @Test
    public void packIntoDirection4(){
        board.loadBoard(new int[][]{{3, 1, 1}, {0, 0, 0}, {0, 0, 0}});
        assertNotNull(board.getTile(1,3));
        assertNotNull(board.getTile(1,2));
        assertNotNull(board.getTile(1,1));
        board.packIntoDirection(Board.Direction.LEFT);
        board.commit();
        assertNotNull(board.getTile(1,1));
        assertNotNull(board.getTile(1,2));
        assertEquals(2,board.getTile(1,2).getRank());
    }
    /*
             *
             *Test if that top side packing works
              */
    @Test
    public void packIntoDirection5(){

        board.loadBoard(new int[][]{{0, 0, 0}, {0, 0, 0}, {1, 1, 2}});
        board.packIntoDirection(Board.Direction.TOP);
        board.commit();

        assertEquals(1,board.getTile(1,1).getRank());
        assertEquals(1, board.getTile(1, 2).getRank());
        assertEquals(2,board.getTile(1,3).getRank());
    }
    /*
             *
             *Test if that top side packing works
              */
    @Test
    public void packIntoDirection6(){
        board.loadBoard(new int[][]{{1, 1, 2}, {0, 0, 0}, {0, 0, 0}});
        board.packIntoDirection(Board.Direction.BOTTOM);
        board.commit();
        assertEquals(1, board.getTile(3, 1).getRank());
        assertEquals(1,board.getTile(3,2).getRank());
        assertEquals(2,board.getTile(3,3).getRank());

    }

    /**
     * Test if we win if we merge (1,1) & (1,2)
     */
    @Test
    public void win(){
        board.loadBoard(new int[][]{{10, 10, 2}, {0, 0, 0}, {0,0, 0}});
        board.packIntoDirection(Board.Direction.LEFT);
        board.commit();
        assertTrue(board.playerJustWon());

    }
    /**
     * Test if we lose if we can't move to any direction
     */
    @Test
    public void lose(){
        board.loadBoard(new int[][]{{10, 9, 1}, {2, 3, 4}, {5,6, 7}});
        board.packIntoDirection(Board.Direction.LEFT);
        board.commit();
        assertTrue(board.playerJustLose());

    }

    /**
     *
     * @param t board
     * @param ti board
     * @return True if the two board are equal
     */
    private boolean Equals(Tile[][] t, Tile[][] ti){
        boolean test=true;
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                if(t[i][j] != null && ti[i][j] == null){
                    test=false;
                }
                if(ti[i][j] != null && t[i][j] == null){
                    test=false;
                }
                if(ti[i][j] != null && t[i][j] != null){
                    if(t[i][j].getRank()!=ti[i][j].getRank()){
                        test=false;
                    }
                }
            }
        }
        return test;
    }

    /**
     * Count the number of none null tile
     * @param t board
     * @return numbre of none null tile
     */
    private int numbreTile(Tile[][] t){
        int i=0;

        for(Tile[] ti : t){
            for(Tile tii : ti){
                if(tii!=null){
                    i++;
                }
            }
    }
        return i;
    }
}
