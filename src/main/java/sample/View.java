package sample;

import controlleur.Controlleur;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Board;
import model.BoardImpl;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.util.logging.Logger;

public class View {
    @FXML
    GridPane grid;
    @FXML
    Text score;
    BoardImpl model = new BoardImpl(4);
    Controlleur controlleur;
    ObservableList<Node> tile;
    Stage stage;

    private static String COLOR_NULL = "#CCC0B2";
    private static String COLOR_2 = "#EEE4DA";
    private static String COLOR_4 = "#ECE0C8";
    private static String COLOR_8 = "#F2B179";
    private static String COLOR_16 = "#F59563";
    private static String COLOR_32 = "#F57C5F";
    private static String COLOR_64 = "#F65D3B";
    private static String COLOR_128 = "#EDCE71";
    private static String COLOR_256 = "#EECC61";
    private static String COLOR_512 = "#ECC850";
    private static String COLOR_1024 = "#EDC53F";
    private static String COLOR_2048 = "#EEC22E";

    public void start(Event event){

       controlleur.recommencer();

    }
    public void init(Stage stage){
        tile=grid.getChildren();
        stage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case LEFT:  controlleur.pivoter("gauche");break;
                    case RIGHT:  controlleur.pivoter("droite");break;
                    case UP: controlleur.pivoter("haut"); break;
                    case DOWN: controlleur.pivoter("bas"); break;
                }
            }
        });

    }

    /*
    *   Set the current Tile Rank
    *
    * */
    public void setRank(int rank,int i,int j){
        String color=getColor(rank);
        ((Label) ((TilePane) tile.get((i * 4) + j)).getChildren().get(0)).setFont(Font.font("Arial Black", 24.0));
        if(rank>0)
            ((Label) ((TilePane) tile.get((i * 4) + j)).getChildren().get(0)).setText((int)Math.pow(2,rank) + "");
        else
            ((Label) ((TilePane) tile.get((i * 4) + j)).getChildren().get(0)).setText("");
        ((TilePane) tile.get((i * 4) + j)).setBackground(new Background(new BackgroundFill(javafx.scene.paint.Paint.valueOf(color), new CornerRadii(20), null)));
    }
    /**
     * Get a color
     *
     * @param rank The tile rank
     *
     */

    private String getColor(int rank){
        String color=COLOR_NULL;
        switch (rank){
            case 1: color = COLOR_2; break;
            case 2: color = COLOR_4; break;
            case 3: color = COLOR_8; break;
            case 4: color = COLOR_16; break;
            case 5: color = COLOR_32; break;
            case 6: color = COLOR_64; break;
            case 7: color = COLOR_128; break;
            case 8: color = COLOR_256; break;
            case 9: color = COLOR_512; break;
            case 10: color = COLOR_1024; break;
            case 11: color = COLOR_2048; break;
        }
        return color;
    }
    public void setControlleur(Controlleur controlleur){
        this.controlleur=controlleur;
    }

    public void setScore(int score){

        this.score.setText("Score: \n"+score+"");
    }
    public void showWinPane(){
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        Scene scene = new Scene(new Group(new Text(25, 25, "You win!!")));
        dialog.setScene(scene);
        dialog.show();
    }
    public void showLosePane(){
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        Scene scene = new Scene(new Group(new Text(25, 25, "You Lose!!")));
        dialog.setScene(scene);
        dialog.show();
        controlleur.recommencer();
    }
}
