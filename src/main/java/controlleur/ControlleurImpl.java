package controlleur;

import javafx.stage.Stage;
import model.Board;
import model.BoardImpl;
import model.Tile;
import sample.View;

import java.util.Random;

/**
 * Created by youness on 07/11/15.
 */
public class ControlleurImpl implements Controlleur{
    BoardImpl model;
    View view;
    Stage stage;
    public ControlleurImpl(Stage stage,View view){
        model=new BoardImpl(4);
        this.view=view;
        this.stage=stage;
        view.init(stage);
        view.setControlleur(this);
        updateView();
    }

    @Override
    public void pivoter(String d) {
        if(d.compareTo("haut")==0){
            model.packIntoDirection(Board.Direction.TOP);
            model.commit();
            updateView();
        }else if(d.compareTo("bas")==0){
            model.packIntoDirection(Board.Direction.BOTTOM);
            model.commit();
            updateView();
        }else if(d.compareTo("gauche")==0){
            model.packIntoDirection(Board.Direction.LEFT);
            model.commit();
            updateView();
        }else if(d.compareTo("droite")==0){
            model.packIntoDirection(Board.Direction.RIGHT);
            model.commit();
            updateView();
        }
    }

    @Override
    public void recommencer() {
        model=new BoardImpl(4);
        updateView();
    }
    private void updateView(){
       view.setScore(model.getScore());
        for(int i=0;i<model.getSideSizeInSquares();i++){
            for(int j=0;j<model.getSideSizeInSquares();j++){
                Tile tile = model.getTile(i+1, j+1);
                int rank=0;
                if(tile!=null){
                    rank=tile.getRank();
                }
                view.setRank(rank,i,j);
            }
        }
        if(model.playerJustWon()){
            view.showWinPane();
        }
        if(model.playerJustLose()){
            view.showLosePane();
        }
    }
}
