# Binôme : Youness AGHAZZAF & Trisna EKO WIYATNI #


### Présentation du TP ###

Le but de Tp est de créer le fameux jeu 2048 en utilisant l'un des Design-pattern: MVC, MVP, MVV ou MVC2. Notre choix s'était mis sur le Design-pattern MVC, car c'est un Design-pattern très utilisé et facile a comprendre. Ainsi MVC est adapté aux applications lourdes voir les jeux vidéos. Donc MVC étais notre premier choix vu que le Tp consiste à faire un jeu.

### Déroulement du tp ###

La premiere partie qu'on a réalisé était de faire le model de l'application. Ce modèle comporte la partie métier de l'application. C'est la partie qui nous a prit beaucoup de temps (1 mois). 
Ensuit, la deuxième était de faire une interface graphique avec JavaFX. On n'a pas programmé l'interface en utilisant le code JavaFX, mais on s'est servi d'un outils (Scene Builder). Cet outil permet de créer facilement des interfaces graphique seulement avec des 'Drag & Drop', et génère un fichier fxml qui comporte notre interface graphique et qui peu être utilisé dans une App JavaFx.
En plus d'une interface graphique, on a fait un lien ou bien un intermédiaire entre notre model et l'interface graphique. On parle bien d'un contrôleur dans une architecture MVC.
La tâche finale était de faire tous les testes possible pour s'assurer du bon fonctionnement de notre model. Donc, on a utilisé Junit pour définir les test à réaliser.

### Execution  ###

Pour exécuter le jeu : jar -cp 2048.jar sample.Main
### Difficulté  ###

Durant le tp, on a rencontré des difficultés qui s'agit du choix du Design-pattern adapté à notre application. On avait aussi du mal à créer une interface graphique à la main juste en tapant du code qui nous a poussé à chercher des outils conçus pour faire ce genre de travail.